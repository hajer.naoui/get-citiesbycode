# get citiesByCode
## Usage
start project `node server.js`

1. Send `GET` request with `code` parameter to http://localhost:3000/api/cities
2. Example for searching city - http://localhost:3000/api/cities?code=66
3. Response will be following:

```json
{
    "status": "ok",
    "data": [
        {
            "id": 1,
            "code": "66178",
            "city": "Saint-Jean-Pla-de-Corts",
        },
        {
            "id": 2,
            "code": "66233",
            "city": "Vivès",
        }
    ]
}
```
