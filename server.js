const express = require('express');
const cities = require('./data/city.list.json');
const app = express();
const port = process.env.PORT || 3000;

const getCityByCode = nameOfCity => cities.filter(({ code }) => new RegExp(nameOfCity, 'i').test(code));
const isValidRequest = request => request && typeof request === 'string';

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});

app.get('/api/cities/', (req, res) => {
    const { code } = req.query;
    isValidRequest(code) ? res.json({ status: 'ok', data: getCityByCode(code) }) : res.json({ status: 'Something goes wrong', data: [] });
});

app.use('*', (req, res) => res.sendStatus(404));

app.listen(port, function() {
    console.log(`Server is running on ${port}`);
});
